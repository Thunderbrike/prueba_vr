﻿using UnityEngine;
using System.Collections;

public class CamaraReflejo : MonoBehaviour {

    public GameObject camera;
    public GameObject reflejo;

    Vector3 camTrans;
    Vector3 target;
    Quaternion rotation;

    void Start ()
    {
        target = new Vector3(0, 0, 0);
        target = reflejo.transform.position;
    }

	void Update ()
    {
        Debug.DrawLine(target, gameObject.transform.position, Color.green);
        
        camTrans = camera.transform.position;
        
        gameObject.transform.position = new Vector3(camTrans.x, -camTrans.y, camTrans.z) + (camera.GetComponent<MouseOrbit>().target + target);
        gameObject.transform.LookAt(target);
        
        //rotation.eulerAngles = new Vector3(0, 0, 180);
        //gameObject.transform.rotation = rotation;
	}
}
