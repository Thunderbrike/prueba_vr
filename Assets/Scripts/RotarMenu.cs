﻿using UnityEngine;
using System.Collections;

public class RotarMenu : MonoBehaviour {

    //public Camera m_Camera;

    Quaternion rotation = Quaternion.identity;

    void Update()
    {
        rotation.eulerAngles = new Vector3(-90, MouseOrbit.rot.eulerAngles.y, 0);
        gameObject.transform.rotation = rotation;
    }
}
