﻿using UnityEngine;
using System.Collections;

public class MouseOrbit : MonoBehaviour
{
    #region Constants
    const int LIMITE_MAXIMO = 88;
    #endregion

    #region Variables
    static public Quaternion rot;
    static public bool laCamaraEstaOrbitando = true;
    static public bool puedoClicarBoton = true;

    public Vector3 target = new Vector3(0, 10, 0);
    public GameObject canvasMenu;
    public GameObject canvasFlecha;
    public GameObject escena;

    public float distance = 5.0f;

    float rotationYAxis = 0.0f;
    float rotationXAxis = 0.0f;
    Vector3 posInicial;
    #endregion

    public static MouseOrbit instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        rotationYAxis = angles.y;
        rotationXAxis = angles.x;
    }

    void LateUpdate()
    {
        EnciendoMenu();

        rotationYAxis = rot.eulerAngles.y;
        rotationXAxis = rot.eulerAngles.x;

        Quaternion fromRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
        Quaternion toRotation = Quaternion.Euler(rotationXAxis, rotationYAxis, 0);
        Quaternion rotation = toRotation;

        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + target;

        transform.rotation = rotation;
        transform.position = position;

    }

    private void EnciendoMenu()
    {
        if (gameObject.transform.position.y <= 0)
        {
            if (puedoClicarBoton)
            {
                canvasMenu.SetActive(true);
                canvasFlecha.SetActive(false);
            }
            else
            {
                canvasMenu.SetActive(false);
                canvasFlecha.SetActive(true);
            }
        }
        else
        {
            if (canvasMenu.activeSelf) canvasMenu.SetActive(false);
            if (canvasFlecha.activeSelf) canvasFlecha.SetActive(false);
            puedoClicarBoton = true;
        }
    }
}