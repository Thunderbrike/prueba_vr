﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
    public enum ModoJuego { VR, AR };
    [Header("Modo de Juego")]
    public ModoJuego modoJuego = ModoJuego.VR;

    [Header("Niveles")]
    public GameObject[] puzzle;
    
    static int numPuzzle = 0;
    static public GameObject modelo;
    static public GameObject modeloSolucionado;
    static public GameObject piezas;
    static public GameObject soportes;

    float counter = 0.0f;
    bool estableceDistancia;
    Vector3 solution;
    public GameObject canvas;
    float oldDistance, newDistance;

    void Start()
    {
        CambioPuzzle(numPuzzle);
    }

    void Update()
    {
        Debug.DrawLine(gameObject.GetComponent<MouseOrbit>().target, gameObject.transform.position, Color.magenta);

        if (estableceDistancia)
        {
            counter += Time.deltaTime;
            oldDistance = gameObject.GetComponent<MouseOrbit>().distance;
            gameObject.GetComponent<MouseOrbit>().distance = Mathf.Lerp(gameObject.GetComponent<MouseOrbit>().distance, newDistance, counter * 0.5f);

            float factorScale = (gameObject.GetComponent<MouseOrbit>().distance * canvas.transform.localScale.x) / oldDistance;
            canvas.transform.localScale = new Vector3(factorScale, factorScale, factorScale);

            if (Mathf.Abs(gameObject.GetComponent<MouseOrbit>().distance - newDistance) < 0.001f)
            {
                estableceDistancia = false;
                counter = 0;
            }
        }
    }

    public void PuzzleSiguiente()
    {
        numPuzzle++;
        if (numPuzzle > puzzle.Length-1) numPuzzle = 0;

        CambioPuzzle(numPuzzle);
    }

    public void PuzzleAnterior()
    {
        numPuzzle--;
        if (numPuzzle < 0) numPuzzle = puzzle.Length-1;

        CambioPuzzle(numPuzzle);
    }

    public void CambioPuzzle(int numPuzzle)
    {
        DesactivoPuzzles();
        puzzle[numPuzzle].SetActive(true);
        CargaVariables(puzzle[numPuzzle]);
        MouseOrbit.puedoClicarBoton = false;
    }

    void DesactivoPuzzles()
    {
        foreach (GameObject n in puzzle)
            n.SetActive(false);
    }

    void CargaVariables(GameObject puzzle)
    {
        // Si estamos en modo VR, tenemos que reposicionar el puzzle según las nuevas coordenadas
        if (modoJuego == ModoJuego.VR)
        {
            puzzle.transform.Find(puzzle.name).transform.position = puzzle.transform.Find(puzzle.name + "VR").transform.position;
            puzzle.transform.Find(puzzle.name).transform.rotation = puzzle.transform.Find(puzzle.name + "VR").transform.rotation;
            soportes = puzzle.transform.Find(puzzle.name + "VR/SoportesVR").gameObject;
            puzzle.transform.Find(puzzle.name + "/Puzzle/Modelo/Soportes").gameObject.SetActive(false);
        }
        else
        {
            soportes = puzzle.transform.Find(puzzle.name + "/Puzzle/Modelo/Soportes").gameObject;
            puzzle.transform.Find(puzzle.name + "VR/SoportesVR").gameObject.SetActive(false);
        }

        // Pasamos las variables del puzzle
        solution = puzzle.transform.Find(puzzle.name + "/Solution").transform.position;
        modelo = puzzle.transform.Find(puzzle.name + "/Puzzle/Modelo").gameObject;
        modeloSolucionado = puzzle.transform.Find("ModeloSolucionado").gameObject;
        piezas = puzzle.transform.Find(puzzle.name + "/Puzzle/Piezas").gameObject;

        soportes.SetActive(true);
        modelo.SetActive(true);
        modeloSolucionado.SetActive(false);
        piezas.SetActive(false);
            
        estableceDistancia = true;
        oldDistance = gameObject.GetComponent<MouseOrbit>().distance;
        newDistance = Vector3.Distance(puzzle.transform.Find(puzzle.name + "/Solution").transform.position, gameObject.GetComponent<MouseOrbit>().target);
    }

}
