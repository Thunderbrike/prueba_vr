﻿using UnityEngine;
using System.Collections;

public class Solution : MonoBehaviour
{
    public GameObject explosion;

    const float TIEMPO_EN_SOLUCION = 2.5f;
    const float TIEMPO_DE_APARICION_SIGUIENTE_PUZZLE = 4f;

	float timeInSolution;
    float timeInitLevel;
    bool flagActivoModeloSolucionado;

    GameObject explosionPosition;

    void Start()
    {
        timeInitLevel = TIEMPO_EN_SOLUCION;
        explosionPosition = gameObject.transform.Find("Explosion").gameObject;
    }

	void OnTriggerEnter (Collider item)		// En cuanto entramos en el collider activamos la cuenta atras
	{
        if (item.tag == "Solution") timeInSolution = timeInitLevel;
	}
	
	void OnTriggerExit (Collider item)		// Si salimos de la solucion inicializamos la cuenta atras
	{
        if (item.tag == "Solution") timeInSolution = timeInitLevel;
	}
	
	void OnTriggerStay (Collider item)		// Mientras estemos dentro de la solucion, restamos el tiempo
	{
        // Si estamos dentro del collider Vamos restando el tiempo hasta agotar el contador
        if (item.tag == "Solution")
        {
            if (timeInSolution > 0)
            {
                timeInSolution -= Time.deltaTime;

            }
            // Si seguimos dentro y el tiempo de contador ha llegado a 0, activamos la solución
            else
            {
                if (!flagActivoModeloSolucionado)
                {
                    flagActivoModeloSolucionado = true;
                    StartCoroutine(MostrarObjetoSolucionado());
                }
            }
        }
	}

    IEnumerator MostrarObjetoSolucionado()
    {
        GameObject clone = Instantiate(explosion, explosionPosition.transform.position, explosionPosition.transform.rotation) as GameObject;
        clone.transform.parent = gameObject.transform;

        yield return new WaitForSeconds(0.5f);

        GameManager.modelo.SetActive(false);
        GameManager.modeloSolucionado.SetActive(true);
        GameManager.piezas.SetActive(true);
        GameManager.soportes.SetActive(false);
        
        StartCoroutine(SiguientePuzzle());
    }

    IEnumerator SiguientePuzzle()
    {
        yield return new WaitForSeconds(TIEMPO_DE_APARICION_SIGUIENTE_PUZZLE);
        flagActivoModeloSolucionado = false;
        GetComponent<GameManager>().PuzzleSiguiente();
    }
}
