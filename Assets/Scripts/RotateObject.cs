﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour {

    public float segundosPorVuelta = 3.0f;

    Quaternion rotation = Quaternion.identity;
    float counter;

	void Start () {
	
	}
	
	void Update () {
        counter += (Time.deltaTime / segundosPorVuelta) * 360; 

        if (counter >= 360) counter = 0;

        rotation.eulerAngles = new Vector3(gameObject.transform.localRotation.x, counter, gameObject.transform.localRotation.y);
        gameObject.transform.localRotation = rotation;
	}
}
